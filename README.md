# Perspective transform example with opencv

![screencast](transform.webm)

Improvements on pyimagesearch example to dynamically modify the crop rectangle

## Credits

- Adrian Rosebrock, The 4 Point OpenCV getPerspectiveTransform Example, PyImageSearch, https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/, accessed on 21 July 2020
