#!/usr/bin/env python

# Usage:

# ./transform_example.py --image images/example_01.png --coords "[(73, 239), (356, 117), (475, 265), (187, 443)]"
# ./transform_example.py --image images/example_02.png --coords "[(101, 185), (393, 151), (479, 323), (187, 441)]"
# ./transform_example.py --image images/example_03.png --coords "[(63, 242), (291, 110), (361, 252), (78, 386)]"

# import the necessary packages
from pyimagesearch.transform import four_point_transform
import numpy as np
import argparse
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
ap.add_argument("-c", "--coords",
	help = "comma seperated list of source points")
args = vars(ap.parse_args())

# load the image and grab the source coordinates (i.e. the list of
# of (x, y) points)
# NOTE: using the 'eval' function is bad form, but for this example
# let's just roll with it -- in future posts I'll show you how to
# automatically determine the coordinates without pre-supplying them
image = cv2.imread(args["image"])
pts = np.array(eval(args["coords"]), dtype = "float32")

p_find = None

def render_image_and_points ():
	clone = image.copy()
	for p in pts:
		cv2.circle(clone, tuple(p), 8, green, -1)
	cv2.imshow("Original", clone)

def move_points(event, x, y, flags, param):
	# grab references to the global variables
	global p_find

	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		if len(pts) < 4:
			print("add points")
		mouse_p = np.array((x,y))
		pos = 0
		for p in pts:
			dist = np.linalg.norm(mouse_p-np.array(tuple(p)))
			if dist < 8:
				p_find = pos
			pos = pos + 1
	elif event == cv2.EVENT_LBUTTONUP:
		p_find = None
	elif event == cv2.EVENT_MOUSEMOVE and flags == cv2.EVENT_FLAG_LBUTTON and p_find is not None:
		pts[p_find] = (x,y)
		render_image_and_points()

green = [0,255, 0]

render_image_and_points()
cv2.setMouseCallback("Original", move_points)

while cv2.getWindowProperty("Original", cv2.WND_PROP_VISIBLE) > 0:
	key = cv2.waitKey(50)
	if key == ord("c"):
		warped = four_point_transform(image, pts)
		cv2.imshow("Warped", warped)

cv2.destroyAllWindows()
